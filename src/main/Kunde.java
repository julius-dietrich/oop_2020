package main;


import main.categories.Fleisch;
import main.categories.Vegetarisch;

public class Kunde extends Observer {
    private double geld;


    public Kunde(Vegetarisch vegetarisch, Fleisch fleisch) {
        this.observed_veg = vegetarisch;
        this.observed_veg.attach(this);
        this.observed_fleisch = fleisch;
        this.observed_fleisch.attach(this);
    }


    @Override
    public void update(String produktName, boolean nachEinkaufVonKundeAusverkauft) {
        if (nachEinkaufVonKundeAusverkauft)
            System.out.println("[" + produktName + "] Dieses Produkt leider nun ausverkauft, du kannst nicht mehr kaufen!");
        else System.out.println("[" + produktName + "] Dieses Produkt ist ausverkauft!");
    }


    public double getGeld() {
        return geld;
    }

    public void setGeld(double geldDifferenz) {
        this.geld = getGeld() + geldDifferenz;
        Overlay.getInstance().updateGuthabenStringInOverlay();


    }
}
