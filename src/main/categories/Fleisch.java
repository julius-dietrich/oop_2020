package main.categories;

import main.flesh.Haenchen;
import main.flesh.Steak;

import main.BuyChecker;
import main.Observer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Fleisch {

    protected String name;
    protected int quality;


    protected List<Haenchen> _haenchen;
    protected List<Steak> _steak;


    private List<Observer> observers = new ArrayList<Observer>();


    public void attach(Observer observer) {
        if (observer instanceof
                BuyChecker) {
            return;
        } else {
            observers.add(observer);
        }
    }

    public void notifyAllObservers(String produktName, boolean nachEinkaufVonKundeAusverkauft) {
        for (Observer observer : observers) {
            observer.update(produktName, nachEinkaufVonKundeAusverkauft);
        }
    }

    public Fleisch() {
        _steak = new ArrayList<>();
        _haenchen = new ArrayList<>();
    }


    public List<Haenchen> get_haenchen() {
        return _haenchen;
    }

    public List<Steak> get_steak() {
        return _steak;
    }


    public void addHaenchen(int k) {
        for (int i = 0; i < k; i++) {
            this.get_haenchen().add(new Haenchen());
        }
    }

    public void addSteak(int k) {
        for (int i = 0; i < k; i++) {
            this.get_steak().add(new Steak());
        }
    }


    public void removeHaenchen() {
        if (!get_haenchen().isEmpty()) {
            this.get_haenchen().remove(get_haenchen().size() - 1);
        }
    }

    public void removeSteak() {
        if (!get_steak().isEmpty()) {
            this.get_steak().remove(get_steak().size() - 1);
        }
    }


    public void printquality() {
//polymorphie

        List<Fleisch> newList = Stream.of(_haenchen, _steak)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
        for (Fleisch b : newList) {
            b.printquality();
        }
        System.out.println("Erzeugte Objekte: " + newList.size());
    }

}
