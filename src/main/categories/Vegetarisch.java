package main.categories;

import main.BuyChecker;
import main.Kunde;
import main.fruits.Apfel;
import main.fruits.Banane;
import main.fruits.Birne;
import main.Observer;
import main.vegetables.Bohnen;
import main.vegetables.Mais;
import main.vegetables.Salat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Vegetarisch {

    protected List<Apfel> _apfel;
    protected List<Banane> _banane;
    protected List<Birne> _birne;
    protected List<Bohnen> _bohnen;
    protected List<Mais> _mais;
    protected List<Salat> _salat;

    private List<Observer> observers = new ArrayList<Observer>();

    public void attach(Observer observer) {
        if (observer instanceof BuyChecker){
            return;
        } else {
            observers.add(observer);
        }
    }

    public void notifyAllObservers(String produktName, boolean nachEinkaufVonKundeAusverkauft) {
        for (Observer observer : observers) {
            observer.update(produktName, nachEinkaufVonKundeAusverkauft);
        }
    }

    public Vegetarisch() {
        _apfel = new ArrayList<>();
        _banane = new ArrayList<>();
        _birne = new ArrayList<>();
        _bohnen = new ArrayList<>();
        _salat = new ArrayList<>();
        _mais = new ArrayList<>();
    }

    public List<Apfel> get_apfel() {
        return _apfel;
    }

    public List<Banane> get_banane() {
        return _banane;
    }

    public List<Birne> get_birne() {
        return _birne;
    }
    public List<Mais> get_mais() {
        return _mais;
    }
    public List<Salat> get_salat() {
        return _salat;
    }

    public List<Bohnen> get_bohnen() {
        return _bohnen;
    }

    public void addApfel(int k) {
        for (int i = 0; i < k; i++) {
            this.get_apfel().add(new Apfel());
        }
    }

    public void addBanane(int k) {
        for (int i = 0; i < k; i++) {
            this.get_banane().add(new Banane());
        }
    }

    public void addBirne(int k) {
        for (int i = 0; i < k; i++) {
            this.get_birne().add(new Birne());
        }
    }
    public void addMais(int k) {
        for (int i = 0; i < k; i++) {
            this.get_mais().add(new Mais());
        }
    }
    public void addBohne(int k) {
        for (int i = 0; i < k; i++) {
            this.get_bohnen().add(new Bohnen());
        }
    }
    public void addSalat(int k) {
        for (int i = 0; i < k; i++) {
            this.get_salat().add(new Salat());
        }
    }
    public void removeMais() {
        if (!get_mais().isEmpty()) {
            this.get_mais().remove(get_mais().size() - 1);
        }
    }
    public void removeBohne() {
        if (!get_bohnen().isEmpty()) {
            this.get_bohnen().remove(get_bohnen().size() - 1);
        }
    }
    public void removeSalat() {
        if (!get_salat().isEmpty()) {
            this.get_salat().remove(get_salat().size() - 1);
        }
    }
    public void removeapfel() {
        if (!get_apfel().isEmpty()) {
            this.get_apfel().remove(get_apfel().size() - 1);
        }
    }


    public void removebanane() {
        if (!get_banane().isEmpty()) {
            this.get_banane().remove(get_banane().size() - 1);
        }
    }

    public void removebirne() {
        if (!get_birne().isEmpty()) {
            this.get_birne().remove(get_birne().size() - 1);
        }
    }

    public void printquality() {

        List<Vegetarisch> newList = Stream.of(_apfel, _banane, _birne, _bohnen ,_mais, _salat)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
        for (Vegetarisch b : newList) {
            b.printquality();
        }
        System.out.println("Erzeugte Objekte: " + newList.size());
    }
}
