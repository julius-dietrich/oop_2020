package main.vegetables;

import main.categories.Gemuese;

public class Bohnen extends Gemuese {
    public Bohnen() {
        super(null);
        super.get_bohnen().add(this);
        quality = (int)((Math.random()) * 6 + 1);
    }
    @Override
    public void printquality(){
        System.out.println("[Bohne] Meine Qualitaet: " + quality);
    }
}
