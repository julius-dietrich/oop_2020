package main.flesh;

import main.categories.Fleisch;

public class Steak extends Fleisch {
    public Steak() {

        super.get_steak().add(this);
        quality = (int) ((Math.random()) * 6 + 1);
    }

    @Override
    public void printquality() {
        System.out.println("[Haenchen] Meine Qualitaet: " + quality);
    }
}
