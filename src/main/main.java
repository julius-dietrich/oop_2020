package main;

import main.categories.Fleisch;
import main.categories.Vegetarisch;

public class main {

    protected static Kunde k;


    public static void main(String[] args) {
        // Comment test für commit und push in VCS
        // Ausgabe Hello World!
        System.out.println("Hello World!");

        Vegetarisch veg = Overlay.getInstance().getVeg();
        Fleisch fleisch = Overlay.getInstance().getFleisch();
        k = new Kunde(veg, fleisch);
        k.setGeld(15);
    }

    public static Kunde getKundeFromMain() {
        return k;
    }
}