package main;


import main.categories.Fleisch;
import main.categories.Vegetarisch;

public abstract  class Observer { //keine instanzen, verteilt funktionen und sorgt dafür das nur erbende zugreifen können
    protected Vegetarisch observed_veg;
    protected Fleisch observed_fleisch;
    public abstract void update(String produktName, boolean nachEinkaufVonKundeAusverkauft);
}
