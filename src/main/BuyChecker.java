package main;

import main.categories.Fleisch;
import main.categories.Vegetarisch;

import static main.main.getKundeFromMain;

public class BuyChecker extends Kunde {

    private final Overlay overlay = Overlay.getInstance();

    public BuyChecker(Vegetarisch subject, Fleisch fleisch) {
        super(subject, fleisch);
    }


    public void zahlen() {

        if (overlay.jcomp25.isSelected() && !observed_veg.get_apfel().isEmpty()) {
            double moneyToRemove = 0;
            for (int i = 0; i < overlay.apfel.getValue(); i++) {
                moneyToRemove = -1;
                if (getKundeFromMain().getGeld() + moneyToRemove < 0) {
                    System.out.println("Du hast zu wenig Geld! Lade es voher auf!");
                    break;
                }
                getKundeFromMain().setGeld(moneyToRemove);
                super.observed_veg.removeapfel();

                if (observed_veg.get_apfel().isEmpty()) {
                    observed_veg.notifyAllObservers("APFEL", true);
                    break;
                }
            }
        } else if (overlay.jcomp25.isSelected() && observed_veg.get_apfel().isEmpty()) {
            observed_veg.notifyAllObservers("APFEL", false);
        }


        if (overlay.check_birne.isSelected() && !observed_veg.get_birne().isEmpty()) {
            double moneyToRemove = 0;
            for (int i = 0; i < overlay.birne.getValue(); i++) {
                moneyToRemove = -1;
                if (getKundeFromMain().getGeld() + moneyToRemove < 0) {
                    System.out.println("Du hast zu wenig Geld! Lade es voher auf!");
                    break;
                }
                getKundeFromMain().setGeld(moneyToRemove);
                super.observed_veg.removebirne();

                if (observed_veg.get_birne().isEmpty()) {
                    observed_veg.notifyAllObservers("BIRNE", true);
                    break;
                }
            }
        } else if (overlay.check_birne.isSelected() && observed_veg.get_birne().isEmpty()) {
            observed_veg.notifyAllObservers("BIRNE", false);
        }


        if (overlay.jcomp28.isSelected() && !observed_veg.get_banane().isEmpty()) {
            double moneyToRemove = 0;
            for (int i = 0; i < overlay.banane.getValue(); i++) {
                moneyToRemove = -2;
                if (getKundeFromMain().getGeld() + moneyToRemove < 0) {
                    System.out.println("Du hast zu wenig Geld! Lade es voher auf!");
                    break;
                }
                getKundeFromMain().setGeld(moneyToRemove);
                super.observed_veg.removebanane();

                if (observed_veg.get_banane().isEmpty()) {
                    observed_veg.notifyAllObservers("BANANE", true);
                    break;
                }
            }

        } else if (overlay.jcomp28.isSelected() && observed_veg.get_banane().isEmpty()) {
            observed_veg.notifyAllObservers("BANANE", false);
        }


        if (overlay.jcomp29.isSelected() && !observed_veg.get_salat().isEmpty()) {
            double moneyToRemove = 0;
            for (int i = 0; i < overlay.salat.getValue(); i++) {
                moneyToRemove = -0.5;
                if (getKundeFromMain().getGeld() + moneyToRemove < 0) {
                    System.out.println("Du hast zu wenig Geld! Lade es voher auf!");
                    break;
                }
                getKundeFromMain().setGeld(moneyToRemove);
                super.observed_veg.removeSalat();

                if (observed_veg.get_salat().isEmpty()) {
                    observed_veg.notifyAllObservers("SALAT", true);
                    break;
                }
            }
        } else if (overlay.jcomp29.isSelected() && observed_veg.get_salat().isEmpty()) {
            observed_veg.notifyAllObservers("SALAT", false);
        }


        if (overlay.jcomp30.isSelected() && !observed_veg.get_mais().isEmpty()) {
            double moneyToRemove = 0;
            for (int i = 0; i < overlay.mais.getValue(); i++) {
                moneyToRemove = -1;
                if (getKundeFromMain().getGeld() + moneyToRemove < 0) {
                    System.out.println("Du hast zu wenig Geld! Lade es voher auf!");
                    break;
                }
                getKundeFromMain().setGeld(moneyToRemove);
                super.observed_veg.removeMais();

                if (observed_veg.get_mais().isEmpty()) {
                    observed_veg.notifyAllObservers("MAIS", true);
                    break;
                }
            }
        } else if (overlay.jcomp30.isSelected() && observed_veg.get_mais().isEmpty()) {
            observed_veg.notifyAllObservers("MAIS", false);
        }


        if (overlay.jcomp31.isSelected() && !observed_veg.get_bohnen().isEmpty()) {
            double moneyToRemove = 0;
            for (int i = 0; i < overlay.bohnen.getValue(); i++) {
                moneyToRemove = -0.5;
                if (getKundeFromMain().getGeld() + moneyToRemove < 0) {
                    System.out.println("Du hast zu wenig Geld! Lade es voher auf!");
                    break;
                }
                getKundeFromMain().setGeld(moneyToRemove);
                super.observed_veg.removeBohne();

                if (observed_veg.get_bohnen().isEmpty()) {
                    observed_veg.notifyAllObservers("BOHNE", true);
                    break;
                }
            }
        } else if (overlay.jcomp31.isSelected() && observed_veg.get_bohnen().isEmpty()) {
            observed_veg.notifyAllObservers("BOHNE", false);
        }

        if (overlay.jcomp33.isSelected() && !observed_fleisch.get_haenchen().isEmpty()) {
            double moneyToRemove = 0;
            for (int i = 0; i < overlay.haenchen.getValue(); i++) {
                moneyToRemove = -5;
                if (getKundeFromMain().getGeld() + moneyToRemove < 0) {
                    System.out.println("Du hast zu wenig Geld! Lade es voher auf!");
                    break;
                }
                getKundeFromMain().setGeld(moneyToRemove);
                super.observed_fleisch.removeHaenchen();

                if (observed_fleisch.get_haenchen().isEmpty()) {
                    observed_fleisch.notifyAllObservers("Haenchen", true);
                    break;
                }
            }
        } else if (overlay.jcomp33.isSelected() && observed_fleisch.get_haenchen().isEmpty()) {
            observed_fleisch.notifyAllObservers("Haenchen", false);
        }

    }


}
