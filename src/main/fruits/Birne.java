package main.fruits;

import main.categories.Obst;

public class Birne extends Obst {
    public Birne() {
        super(null);
        super.get_birne().add(this);
        quality = (int)((Math.random()) * 6 + 1);
    }
    @Override
    public void printquality(){
        System.out.println("[Birne] Meine Qualitaet: " + quality);
    }
}
