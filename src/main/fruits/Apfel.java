package main.fruits;
import main.categories.Obst;

public class Apfel extends Obst {

    public Apfel(){
        super(null);
        super.get_apfel().add(this);
        quality = (int)((Math.random()) * 6 + 1);
    }
    @Override
    public void printquality(){
        System.out.println("[Apfel] Meine Qualitaet: " + quality);
    }
}
